INSTALL_DIR="/home/pi/manage-car"
SUDO=''
if (( $EUID != 0 )); then
    SUDO='sudo'
fi

$SUDO apt-get update
$SUDO apt-get install -y python-smbus i2c-tools
$SUDO apt-get install -y python python-pip
$SUDO pip install pathlib

# batmobile dependecies installing.
$SUDO apt-get install -y build-essential python3 python3-dev python3-virtualenv python3-numpy python3-picamera python3-pandas python3-rpi.gpio i2c-tools avahi-utils joystick libopenjp2-7-dev libtiff5-dev gfortran libatlas-base-dev libopenblas-dev libhdf5-serial-dev git
wget https://github.com/lhelontra/tensorflow-on-arm/releases/download/v1.13.1/tensorflow-1.13.1-cp35-none-linux_armv7l.whl
pip install tensorflow-1.13.1-cp35-none-linux_armv7l.whl

mkdir provision

# Make all bash scripts executable
find ${INSTALL_DIR}/apps/ -type f -iname "*.sh" -exec chmod +x {} \;
$SUDO chmod +x main.py

[ -f config.json ] || cp config.json.example config.json
cd $INSTALL_DIR
$SUDO cp manage-car.service /etc/systemd/system/
$SUDO systemctl daemon-reload
$SUDO systemctl enable manage-car.service

# clone the donkey car repo
cd /home/pi
$SUDO -u pi git clone https://gitlab.com/futuracer/batmobile.git donkey
$SUDO chown -R pi:pi /home/pi/batmobile
$SUDO -u pi python3 -m virtualenv -p python3 env --system-site-packages
$SUDO -u pi pip install -e donkey[pi]
donkey createcar --path ~/batmobile


#Start it maybe?\