import glob
import subprocess

from pathlib import Path

import config
from ui import Menu, DialogBox, Printer, format_for_screen, Listbox

menu_name = "Drive"

i = None  # Input device
o = None  # Output device


def callback():
    drive_menu = [
        ['Bluetooth', drive_bluetooth],
        ['Webserver', drive_web],
        ['Clean', clean],
        ['Model', drive_model]
    ]

    Menu(drive_menu, i, o, "batmobile drive interface").activate()


def init_app(input, output):
    global i, o
    i = input
    o = output


def clean():
    donkey_path = '/home/pi/env/bin/donkey'
    drive_path = '/home/pi/batmobile'

    log_file = open(drive_path + "/_log.txt", "w")

    exec_command = 'exec {} tubclean {}'.format(donkey_path, drive_path)

    try:
        p = subprocess.Popen(exec_command, stdout=log_file, shell=True)
        Printer(format_for_screen('Clean server loading: Port 8886', o.cols, False), i, o, 5, True)
    except OSError as e:
        if e.errno == 2:
            Printer("File not found!", i, o, 1)
        elif e.errno == 13:
            Printer(["Permission", "denied!"], i, o, 1)
        else:
            Printer("Unknown error!", i, o, 1)
    finally:
        answer = DialogBox("yn", i, o, message="Close Server").activate()
        if answer:
            frames = len(glob.glob1(drive_path + '/tub', "*.jpg"))
            message = "Frames: {}".format(frames)
            Printer(message, i, o, 5, False)
            p.kill()
            log_file.close()


def drive_bluetooth():
    drive('drive')


def drive_web():
    drive('drive')


def drive_model():
    model_folder = config.CAR_PATH + '/models'
    models = glob.glob(model_folder + '/*')
    model_select = []
    Printer('Select Type', i, o, 1, False)
    listbox_contents = [
    ["linear", "linear"],
    ["categorical", "categorical"],
    ["rnn", "rnn"],
    ["3d", "3d"],
    ["latent", "latent"]]

    type_selected = Listbox(listbox_contents, i, o).activate()


    for model in models:
        car_path_length = len(model_folder)
        model_select.append([
            model[car_path_length:], lambda x=model: drive('drive --model {} --type {}'.format(model, type_selected))
        ])

    Menu(model_select, i, o, "batmobile drive interface").activate()


def drive(arguments):
    python_path = '/home/pi/tenv/bin/python'
    drive_path = '/home/pi/batmobile'
    log_file = open(drive_path + "/_log.txt", "w")

    exec_command = 'exec {} {}/manage.py {}'.format(python_path, drive_path, arguments)

    try:
        p = subprocess.Popen(exec_command, stdout=log_file, shell=True)
        Printer('Drive loading', i, o, 7, True)
    except OSError as e:
        if e.errno == 2:
            Printer("File not found!", i, o, 1)
        elif e.errno == 13:
            Printer(["Permission", "denied!"], i, o, 1)
        else:
            Printer("Unknown error!", i, o, 1)
        output = ""
    finally:
        answer = DialogBox("yn", i, o, message="Finish Session?").activate()
        if answer:
            frames = len(Path(drive_path + '/drive').glob("**/*.jpg"))
            message = "Frames: {}".format(frames)
            Printer(message, i, o, 5, False)
            p.kill()
            log_file.close()
