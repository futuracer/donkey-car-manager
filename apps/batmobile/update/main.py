import os

from config import CAR_PATH

menu_name = "Update Batmobile"


from subprocess import check_output, CalledProcessError

from ui import Printer

i = None  # Input device
o = None  # Output device


def init_app(input, output):
    global i, o
    i = input
    o = output

def callback():
    Printer("Updating...", i, o, 1)
    try:
        os.chdir(CAR_PATH)
        output = check_output(["git", "fetch", "origin", "--prune"])
        if "Already up-to-date." in output:
            Printer("All up-to-date", i, o, 1)
        else:
            Printer("Updated project", i, o, 1)
    except OSError as e:
        if e.errno == 2:
            Printer("git not found!", i, o, 1)
        else:
            Printer("Unknown OSError!", i, o, 1)
    except CalledProcessError as e:
        if e.returncode == 1:
            Printer(["Can't connect", "to GitHub?"], i, o, 1) #Need to check output - can also be "Conflicting local changes" and similar
        else:
            Printer(["Failed with", "code {}".format(e.returncode)], i, o, 1)