#!/bin/bash

echo "\n======================="
echo "Installing Dependencies!"
echo "======================="




if sudo python -c 'import pkgutil; exit(not pkgutil.find_loader("glob"))'; then
    echo 'glob found'
else
    echo 'glob not found'
    echo "Installing glob"
    sudo pip install glob google-cloud-storage
fi

if sudo python -c 'import pkgutil; exit(not pkgutil.find_loader("zipfile"))'; then
    echo 'zipfile found'
else
    echo 'zipfile not found'
    echo "Installing zipfile"
    sudo pip install zipfile shutil
fi

