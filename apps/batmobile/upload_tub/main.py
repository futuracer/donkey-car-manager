menu_name = "Upload"

from google.cloud import storage
import os
import shutil
import datetime
import config

from ui.printer import Printer

TUB_PATH = config.CAR_PATH + '/data'
CAR_PATH = config.CAR_PATH
currentDT = datetime.datetime.now()

CAR_NAME = config.CAR_NAME


def upload_to_bucket(blob_name, path_to_file, bucket_name):
    """ Upload data to a bucket"""

    # Explicitly use service account credentials by specifying the private key
    # file.
    storage_client = storage.Client.from_service_account_json(
        '/home/pi/service_account.json')

    # print(buckets = list(storage_client.list_buckets())

    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(blob_name)
    blob.upload_from_filename(path_to_file)

    # returns a public url
    return blob.public_url


def callback():
    outPutname = "data_" + currentDT.strftime("%d.%m_%H_%M_%S")

    Printer("Starting compression and upload", i, o, 1)

    output_zip_path = config.CAR_PATH + '/' + outPutname

    shutil.make_archive(base_name=output_zip_path, format='zip', root_dir=TUB_PATH)

    Printer(["(1/2)", "File compressed!"], i, o, 1)

    file_name = '{}/{}'.format(CAR_NAME, outPutname)

    upload_to_bucket(file_name, output_zip_path + '.zip', 'futurace-hki-frames')

    os.remove(outPutname)

    Printer(["(2/2)", "File uploaded!"], i, o, 1)


i = None  # Input device
o = None  # Output device


def init_app(input, output):
    global i, o
    i = input;
    o = output
