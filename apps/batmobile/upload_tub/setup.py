from helpers.setup_manager import ProvisionHelper
from config import MANAGE_PATH


def setup():
    setup_bluetooth = ProvisionHelper('upload_tub', MANAGE_PATH + '/apps/batmobile/upload_tub/setup.sh')
    setup_bluetooth.setup()
