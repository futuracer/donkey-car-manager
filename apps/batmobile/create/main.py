import subprocess

import config
from ui import Menu, DialogBox, Printer, format_for_screen, MenuExitException

menu_name = "Create Car"

i = None  # Input device
o = None  # Output device


def init_app(input, output):
    global i, o
    i = input
    o = output


def callback():
    donkey_path = '/home/pi/tenv/bin/donkey'
    drive_path = config.CAR_PATH

    answer = DialogBox("yn", i, o, message="Are you sure. This will destroy any existing files").activate()

    # execute create car command
    # donkey createcar --path ~/d2
    exec_command = 'exec {} createcar --path {}'.format(donkey_path, drive_path)

    if answer:
        try:
            remove_folder = subprocess.Popen('rm -rf {}'.format(drive_path))
            Printer('Removed car', i, o, 1, True)
            p = subprocess.Popen(exec_command, stdout=subprocess.PIPE, shell=True)
            for line in p.stdout:
                Printer(format_for_screen(format_for_screen(line), o.cols, False), i, o, 1, True)
        except Exception as e:
            Printer(format_for_screen(format_for_screen(e), o.cols, False), i, o, 1, True)
            raise MenuExitException
