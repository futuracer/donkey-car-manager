import config
from ui import Printer
from google.cloud import storage

menu_name = "Get Models"

i = None  # Input device
o = None  # Output device

# Explicitly use service account credentials by specifying the private key
# file.
storage_client = storage.Client.from_service_account_json(
    '/home/pi/service_account.json')

def init_app(input, output):
    global i, o
    i = input
    o = output


def callback():
    Printer("Updating...", i, o, 1)

    bucket_name = config.GOOGLE_STORAGE_MODEL_BUCKET

    model_keys = list_blobs(bucket_name)

    for key in model_keys:
        output_model_path = config.CAR_PATH + '/models/' + key
        download_blob(bucket_name, key, output_model_path)


def download_blob(bucket_name, source_blob_name, destination_file_name):
    """Downloads a blob from the bucket."""
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(source_blob_name)

    blob.download_to_filename(destination_file_name)

    print('Blob {} downloaded to {}.'.format(
        source_blob_name,
        destination_file_name))


def list_blobs(bucket_name):
    """Lists all the blobs in the bucket."""
    bucket = storage_client.get_bucket(bucket_name)

    blobs = bucket.list_blobs()

    blob_keys = []

    for blob in blobs:
        blob_keys.append(blob.name)

    return blob_keys
