import importlib
import os

from apps.manager import app_walk


class SetupManager():
    def __init__(self, app_directory, menu_class, printer_func, i, o):
        self.app_directory = app_directory
        self.menu_class = menu_class

        self.printer_func = printer_func
        self.i = i
        self.o = o

    def setup_all_apps(self):
        for path, subdirs, modules in app_walk(self.app_directory):
            for module in modules:
                # Then, we load modules and store them along with their paths
                try:
                    module_path = os.path.join(path, module)
                    app = self.load_app(module_path)
                    print("Setup app {}".format(module_path))
                except Exception as e:
                    print("Failed to find setup script {}".format(module_path))
                    print(e)
                    self.printer_func(["Setup skipped", os.path.split(module_path)[1]], self.i, self.o, 2)

    def load_app(self, app_path):
        app_import_path = app_path.replace('/', '.')
        app = importlib.import_module(app_import_path + '.setup', package='apps')
        self.printer_func(["Setup", os.path.split(app_path)[1]], self.i, self.o, 2)
        if app.setup():
            self.printer_func(["Setup", "Complete"], self.i, self.o, 2)
        return app
