import time

menu_name = "Bluetooth"

from apps.bluetooth_controller.drivers.Bluetoothctl import Bluetoothctl

from ui import Printer, Menu
import re

# Some globals for LCS
main_menu = None
# Some globals for us
i = None
o = None


def scan():
    Printer("Ready!", None, o, 0)
    bl.start_scan()
    Printer("Scanning for 10 seconds...", None, o, sleep_time=10)

    list_of_devices = bl.get_available_devices()
    listbox = []
    for index, item in enumerate(list_of_devices):
        if re.search('controller', item['name'], re.IGNORECASE):
            listbox.append([item['name'], lambda x=item: pair(x['mac_address'])])

    main_menu = Menu(listbox, i, o, "Show scanned results", catch_exit=False)
    main_menu.activate()


def attempt_pair(mac):
    bl.remove(mac)
    time.sleep(1)
    if bl.connect(mac):
        return bl.trust(mac)
    return False


def pair(mac):
    Printer('paring', i, o, 1, skippable=False)
    bl.start_agent()
    bl.default_agent()
    if not bl.connect(mac):
        bluetooth_paired = False

        attempt_count = 1

        while not bluetooth_paired:
            Printer(['paring...', 'Attempt: ' + str(attempt_count)], i, o, 1, skippable=False)
            bluetooth_paired = attempt_pair(mac)
            attempt_count += 1

    Printer('Paired Success', i, o, 3, skippable=True)


def callback():
    main_menu_contents = []
    main_menu_contents.append(["Scan", scan])
    main_menu = Menu(main_menu_contents, i, o, "Bluetooth tools")
    main_menu.activate()


def init_app(input, output):
    global i, o, bl, main_menu, callback
    bl = Bluetoothctl()
    i = input;
    o = output  # Getting references to output and input device objects and saving them as globals
