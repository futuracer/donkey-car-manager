from helpers.setup_manager import ProvisionHelper
from config import MANAGE_PATH

def setup():
    setup_bluetooth = ProvisionHelper('bluetooth_controller', MANAGE_PATH + '/apps/bluetooth_controller/setup.sh')
    print('running bluetooth setup.')
    return setup_bluetooth.setup()
