#!/bin/bash

sudo pip install pexpect

sudo apt-get -y install bluetooth
sudo apt install -y bluetooth libbluetooth-dev pkg-config libboost-python-dev libboost-thread-dev libglib2.0-dev
python-dev

sudo echo "options bluetooth disable_ertm=Y" | sudo tee /etc/modprobe.d/bluetooth.conf

echo "setup complete!!!"