

# Device Identity
DEVICE_NAME = 'DevelopmentPi'
HOST_NAME = 'donkey_pi_dev'
REPO_SOURCE = 'https://gitlab.com/futuracer/donkey-car-manager'

# Setup
SETUP_VERSION = 1
MANAGE_PATH = '/home/pi/manage-car'
CAR_PATH = '/home/pi/batmobile'
CAR_NAME = 'hell-jee'

GOOGLE_STORAGE_MODEL_BUCKET = 'futurace-hki-models'