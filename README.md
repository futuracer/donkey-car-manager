# Manage FutuRace

## Summery

Service that will be responsible for managing the raspberry pi. This purpose of the project is to create a service on the Pi that will run on every boot.

## Installation
For this project to work the repo must be copied to the target raspberry pi and the `setup.sh` must be run.

You also need to have a google service account if you expect the upload app to work.

Place the service account in
```.env
/home/pi/service_account.json
```

## Development
**USING** Pycharm setup a remote interpreter. 

Open Pycharm setting and select the 'Project Interpreter'

![setting](https://i.imgur.com/FhwVuoo.png)

Click on the gear then Add

![add](https://i.imgur.com/cV9vG73.png)

On the left select *SSH Interpreter*

![ssh](https://i.imgur.com/7yAJyNC.png)

Fill out the info. For *Host* use the IP of the Raspberry Pi and *Username* is `pi`. Click next. Password is `raspberry` 

**NOTE:** If you are not on the same network pycharm will complain.

Finally add the correct python path in this case `home/pi/env/bin/python`

![python](https://i.imgur.com/VPl8hAJ.png)

 