import glob
import socket

from lcd_screen.main import LcdScreen


def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


def get_number_of_frames():
    return len(glob.glob1('/home/pi/mycar/tub', "*.jpg"))

