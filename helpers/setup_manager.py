import subprocess
from pathlib import Path
import os
from config import MANAGE_PATH


class ProvisionHelper:
    def __init__(self, part_name, provision_script):
        self._part_name = part_name
        self._provision_artifact = str(MANAGE_PATH + '/provision/' + self._part_name)
        self._provision_script = provision_script

    def setup_not_completed(self):
        exists = os.path.isfile(self._provision_artifact)
        return not exists

    def setup(self):
        if self.setup_not_completed():
            log_file = open(self._provision_artifact + "_log.txt", "w")
            is_finished = subprocess.call(self._provision_script, shell=True, stdout=log_file)
            if is_finished == 0:
                subprocess.check_call(['touch', self._provision_artifact])
                log_file.close()
                print('Setup is complete for ' + self._part_name)
                return True
            return False

        print(self._part_name + ' has been setup.')
        return True

    def reset(self):
        ## If file exists, delete it ##
        if os.path.isfile(self._provision_artifact):
            os.remove(self._provision_artifact)
        else:  ## Show an error ##
            print("Error: %s file not found" % self._provision_artifact)